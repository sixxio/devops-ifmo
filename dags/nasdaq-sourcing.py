import requests as rq
import pandas as pd
import sqlalchemy as sa
import json
import pendulum
from airflow.decorators import dag, task

headers = {"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                        "Accept-Encoding":"gzip, deflate",
                        "Accept-Language":"en-GB,en;q=0.9,en-US;q=0.8,ml;q=0.7",
                        "Connection":"keep-alive",
                        "Referer":"https://www.nasdaq.com/market-activity/quotes/historical",
                        "Upgrade-Insecure-Requests":"1",
                        "User-Agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36"}    

@dag(start_date=pendulum.now(), schedule='@daily')
def load_new_data_from_NASDAQ():
    
    @task()
    def fetch_last_data():
        response = rq.get('https://charting.nasdaq.com/data/charting/intraday?symbol=AAPL&mostRecent=5&includeLatestIntradayData=1&', headers=headers, verify=True)
        fetched = pd.DataFrame(json.loads(response.text)['marketData'])
        fetched['Date'] = pd.to_datetime(fetched['Date'])
        return fetched

    @task()
    def save_into_postgres(fetched):
        engine = sa.create_engine('postgresql://airflow:airflow@postgres/airflow')
        with engine.connect() as connection:
            connection.execute(sa.text('create schema if not exists stocks_prices;'))
            fetched.to_sql(con= connection,
                    name='aapl_prices',
                    schema='stocks_prices',
                    if_exists='append',
                    index=False)

    save_into_postgres(fetch_last_data())

load_new_data_from_NASDAQ()
